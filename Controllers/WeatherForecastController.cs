using dotnetcicd.Models;
using dotnetcicd.Services;

using Microsoft.AspNetCore.Mvc;
using StackExchange.Redis;

using System.Collections.Generic;
using System.Text.Json;

namespace dotnetcicd.Controllers
{
	[ApiController]
	[Route("[controller]")]
	public class WeatherForecastController : ControllerBase
	{
		private static readonly string[] Summaries = new[]
		{
		"Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
	};

		private readonly ILogger<WeatherForecastController> _logger;
		private RedisService _redisService;

		public WeatherForecastController(ILogger<WeatherForecastController> logger, RedisService redisService)
		{
			_logger = logger;
			_redisService = redisService;
		}

		[HttpGet(Name = "GetWeatherForecast")]
		public IEnumerable<WeatherForecast> Get()
		{
			return Enumerable.Range(1, 5).Select(index => new WeatherForecast
			{
				Date = DateTime.Now.AddDays(index),
				TemperatureC = Random.Shared.Next(-20, 55),
				Summary = Summaries[Random.Shared.Next(Summaries.Length)]
			})
			.ToArray();
		}

		[HttpGet]
		[Route("Redis/Get")]
		public async Task<IActionResult> GetValueByKey(string key)
		{ 
			try
			{
				return Ok(new { value1 = await _redisService.GetValueAsync(key) });
			}
			catch (Exception ex)
			{
				return Ok(new { value1 = ex.Message});
			}
		}

		[HttpPost]
		[Route("Redis/Set")]
		public async Task<IActionResult> GetValueByKey([FromBody] RedisRequest<string> request)
		{
			try
			{
				var response = await _redisService.SetValueAsync(request);
				return Ok(new { value1 = response });
			}
			catch (Exception ex)
			{
				return Ok(new { value1 = ex.Message });
			}
		}


		[HttpPost]
		[Route("Redis/SetTopService")]
		public async Task<IActionResult> SetKey([FromBody] RedisRequest<TopService> request)
		{
			try
			{
				RedisRequest<string> _request = new() { Key = request.Key, Value = JsonSerializer.Serialize(request.Value)};
				var response = await _redisService.SetValueAsync(_request);
				return Ok(new { value1 = response });
			}
			catch (Exception ex)
			{
				return Ok(new { value1 = ex.Message });
			}
		}

		[HttpGet]
		[Route("GetStrWeatherForecast")]
		public async Task<IActionResult> GetSsssss()
		{
			string key1 = "GetAllBankCardActive";
			string key2 = "GetServicesActive";

			try
			{
				var options = ConfigurationOptions.Parse("203.161.43.55");
				options.Password = "123456789";
				options.ConnectTimeout = 10000;
				ConnectionMultiplexer redis = ConnectionMultiplexer.Connect(options);
				IDatabase db = redis.GetDatabase();
				string _value = db.StringGet(key1);
				string _value2 = db.StringGet(key2);
				
				return Ok(new { value1 = _value, value2 = _value2 });
			}
			catch (Exception ex)
			{
				return Ok("failed");
			}

		}
	}

	//var demoObjTopService = [
 //       { id: 1, carName: '30K-55555', countContract: 51122, totalMoney: 5666514572 },
 //       { id: 2, carName: '30E-68686', countContract: 43123, totalMoney: 4612514432 },
 //       { id: 3, carName: '30A-99999', countContract: 27899, totalMoney: 3944326125 },
 //       { id: 4, carName: '30E-42104', countContract: 20534, totalMoney: 3134125326 },
 //       { id: 5, carName: '68A-14842', countContract: 506, totalMoney: 2731425675 },
 //       { id: 6, carName: '13A-54112', countContract: 300, totalMoney: 1925622375 },
 //       { id: 7, carName: '18A-44121', countContract: 106, totalMoney: 1142115567 },
 //   ];

	public class TopService
	{
        public int Id { get; set; }
        public string? CarName { get; set; }
        public long CountContract { get; set; }
        public decimal TotalMoney { get; set; }
    }

}