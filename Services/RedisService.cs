﻿using dotnetcicd.Models;

using StackExchange.Redis;

using System.Text.Json;
using System.Text.Json.Nodes;

namespace dotnetcicd.Services
{
	public class RedisService
	{
		private readonly ConnectionMultiplexer _redis;

		public RedisService(string host, string password, int connectTimeout)
		{
			var options = ConfigurationOptions.Parse(host);
			options.Password = password;
			options.ConnectTimeout = connectTimeout;

			_redis = ConnectionMultiplexer.Connect(options);
		}

		public async Task<RedisResponse<string>> GetValueAsync(string key)
		{
			try
			{
				RedisResponse<string> response = new();
				IDatabase db = _redis.GetDatabase();
				var value = await db.StringGetAsync(key);
				response.SetSuccess();
				response.Value = value;
				return response;
			}
			catch (Exception ex)
			{
				return new RedisResponse<string>() { Message = ex.Message };
			}
		}


		public async Task<RedisResponse<bool>> SetValueAsync(RedisRequest<string> request)
		{
			try
			{
				RedisResponse<bool> response = new();
				IDatabase db = _redis.GetDatabase();
				var value = await db.StringSetAsync(request.Key, request.Value);
				response.SetSuccess();
				response.Value = true;
				return response;
			}
			catch (Exception ex)
			{
				return new RedisResponse<bool>() { Message = ex.Message};
			}
		}

		public async Task<RedisResponse<bool>> ClearKeyAsync(string key)
		{
			try
			{
				RedisResponse<bool> response = new();
				IDatabase db = _redis.GetDatabase();
				var value = await db.KeyDeleteAsync(key);
				response.SetSuccess();
				response.Value = true;
				return response;
			}
			catch (Exception ex)
			{
				return new RedisResponse<bool>() { Message = ex.Message };
			}
		}


	}
}
