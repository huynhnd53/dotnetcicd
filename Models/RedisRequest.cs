﻿namespace dotnetcicd.Models
{
	public class RedisRequest<T>
	{
        public string Key { get; set; }
        public T Value { get; set; }
    }
}
