﻿namespace dotnetcicd.Models
{
	public class RedisResponse<T>
	{
		public string Message { get; set; } = "Không tìm thấy key";
        public int Status { get; set; } = 0;
		public T? Value { get; set; }

	 	public void SetSuccess()
		{
			Message = "Thành công!";
			Status = 1;
		}
	}
}
