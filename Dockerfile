﻿
## Use the .NET 6 SDK image as a build environment
#FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build-env
#WORKDIR /app
#
## Copy the project file and restore dependencies
#COPY *.csproj ./
#RUN dotnet restore
#
## Copy the remaining source code
#COPY . ./
#
## Build the application
#RUN dotnet publish -c Release -o out
#
## Build the runtime image
#FROM mcr.microsoft.com/dotnet/aspnet:6.0
#WORKDIR /app
#COPY --from=build-env /app/out .
#
##nếu muốn prj chạy trên PORT chỉ định của container (container của project) thì cần cấu hình phần này 
##nếu không prj chạy trên PORT mặc định là 80 
## Expose port 80 to the outside world
#EXPOSE 80
#
## Define the entry point for the application
#ENTRYPOINT ["dotnet", "dotnetapp.dll"]
#





FROM mcr.microsoft.com/dotnet/sdk:6.0-alpine as build
WORKDIR /app
COPY . .
RUN dotnet restore
RUN dotnet publish -o /app/published-app

FROM mcr.microsoft.com/dotnet/aspnet:6.0-alpine as runtime
WORKDIR /app
COPY --from=build /app/published-app /app

EXPOSE 80

ENTRYPOINT ["dotnet", "/app/dotnetapp.dll"]